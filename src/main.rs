use warp::Filter;

mod handlers;
mod routes;

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt().pretty().init();

    let cors = warp::filters::cors::cors().allow_any_origin();
    let router = routes::api_routes().with(cors).with(warp::trace::request());

    warp::serve(router).run(([127, 0, 0, 1], 8080)).await
}
