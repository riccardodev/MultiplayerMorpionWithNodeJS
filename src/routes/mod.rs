use warp::{filters::ws::Ws, Filter};

use crate::handlers::ws::ws_hanlder;

pub fn api_routes() -> impl Filter<Extract = (impl warp::Reply,), Error = warp::Rejection> + Clone {
    let ws_route = warp::get()
        .and(warp::ws())
        .then(|ws: Ws| async { ws.on_upgrade(ws_hanlder) });
    ws_route
}
